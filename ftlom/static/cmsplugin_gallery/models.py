from django.db import models
from django.db.models import fields
from filer.fields.image import FilerImageField
from cms.models import CMSPlugin


class Gallery(CMSPlugin):
    image = FilerImageField()
    label = models.CharField(null=True, blank=True, max_length=255)