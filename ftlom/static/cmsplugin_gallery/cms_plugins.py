from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from models import Gallery

class GalleryPlugin(CMSPluginBase):
    model = Gallery
    name = "Gallery"
    render_template = "cmsplugin_gallery.html"
        
    def render(self, context, instance, placeholder):
        context.update({
            'instance': instance
        })
        return context

plugin_pool.register_plugin(GalleryPlugin)